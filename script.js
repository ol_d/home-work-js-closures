
// Создайте функцию user, сделайте так чтобы функция выводила в консоль контекст
function user() {
    console.log(this);
}


// Создайте обьект userOne, добавьте к нему свойства имя, фамилия, возраст и функцию, которая будет выводить  Привет! Я имя + фамилия  Выведите имя и фамилию пользователя с объекта userOne.
// Сделайте так, чтобы контекст в методе объекта userOne был глобальный объект window.
const userOne = {
    name: 'Karl',
    surname: 'XII',
    greeting: function() {
        alert(`Привет! Я ${this.name} ${this.surname}`);
    },
    globalObjectWindow: user.bind(window)
}


// Создайте еще один объект userTwo и заполните его теми же свойствами, но без метода.
const userTwo = {
    name: 'Ivan',
    surname: 'Ivanov'
}


// Выведите информацию с объекта userTwo используя метод объекта userOne.
userOne.greeting.call(userTwo);


// Создайте метод который будет умножать элементы массива на то число которое будет передавать пользователь. Сделайте так, чтобы метод наследовался каждым массивом подобно методу pop().
Array.prototype.mArray = function(x){
    let i = 0; 
    this.forEach((e) => {
    this[i] = e + x;
    i++;
   })
    return this;
}


/* 
Сделайте функцию, которая считает и выводит количество своих вызовов.
                    func(); //выведет 1
                    func(); //выведет 2
                    func(); //выведет 3
                    func(); //выведет 4
*/
let foo1 = function() {
    let counter = 0;
    function foo2() {
        counter++;
        console.log(counter);
    }
    return foo2;
}
let countNumberOfCalls = foo1();


// Сделайте функцию, каждый вызов который будет генерировать случайные числа от 1 до 100, но так, чтобы они не повторялись, пока не будут перебраны все числа из этого промежутка. Решите задачу через замыкания - в замыкании должен хранится массив чисел, которые уже были сгенерированы функцией.
function numbersGen() {
    let counter = 0;
    const arr = [];
    function foo() {
        while (counter < 100){
            let num = Math.floor(Math.random() * 100)+1;
            if (!arr.includes(num)){
                arr.push(num);
                counter++;
            }
        }
        return arr;
    }
    return foo;
}

const ansv = numbersGen();

console.log(ansv());
